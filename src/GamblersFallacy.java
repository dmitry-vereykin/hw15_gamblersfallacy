/**
 * Created by Dmitry on 6/20/2015.
 */
import java.util.Random;

public class GamblersFallacy {
    private static int TOSSES = 8000;
    private static int sideUp;
    private static int hCount;
    private static int tCount;
    private static int count;
    private static int rowCount;
    private static final int HEADS = 0;
    private static final int TAILS = 1;
    private static int[] array = new int[TOSSES];

    private GamblersFallacy() {
        sideUp = 0;
        hCount = 0;
        tCount = 0;
        count = 0;
        rowCount = 0;
    }

    public static void main(String[] args) {
        GamblersFallacy coin = new GamblersFallacy();

        for (int i = 0; i < TOSSES; i++) {
            coin.toss();
            array[i] = getSideUp();
            //System.out.print(array[i] + " ");
        }

        for (int i = 0; i < TOSSES - 1; i++) {
            if (array[i] == TAILS)
                count++;
            else
                count = 0;

            if (count == 4 && array[i + 1] == TAILS) {
                tCount++;
                rowCount++;
            }

            if (count == 4 && array[i + 1] == HEADS) {
                hCount++;
                rowCount++;
            }
        }
        System.out.println("-------------------------------------");
        System.out.println("In " + TOSSES + " tosses, four tails in a row\noccurred " + getRowCount() + " times. " +
                "After four\ntails in a row, the next toss was: " + "\n- A head " + getHeadsCount() +" times, and" +
                "\n- A tail " + getTailsCount() + " times.");
    }

    private void toss() {
        Random rand = new Random();
        int side = rand.nextInt(2);
        if (side == 0)
            sideUp = HEADS;
        else
            sideUp = TAILS;
    }

    private static int getSideUp() {
        return sideUp;
    }

    private static int getRowCount(){
        return rowCount;
    }

    private static int getHeadsCount() {
        return hCount;
    }

    private static int getTailsCount() {
        return tCount;
    }
}